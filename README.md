# tradecore-fe-test

To start the project, clone this repository and run

```
npm install
```

or

```
yarn
```

from inside the project folder. Once that finishes,
```
npm start
```
will start it in development mode, and
```
npm build
```
will bundle an uglified production version.

*A few notes*

Declaring anonymous functions from within render methods has known performance
drawbacks and should not be done in production-ready builds. The use of `recompose`
would eliminate the need for them while also reducing the number of verbose class definitions.
