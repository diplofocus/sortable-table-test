// @flow

import * as React from 'react';
import { connect } from 'react-redux';
import { compose, type Dispatch } from 'redux';

import Error404 from '../components/Error404';
import { getTable, sortData, filterData } from '../actions/tableActions';
import type { TableData, State as StoreState } from '../reducers/tableReducer';

type Props = {
  data: ?TableData,
  getTable: string => void,
  sortData: (tableName: string, columnName: string, order: 'ascending' | 'descending') => void,
  filterData: (tableName: string, columnName: string, value: string) => void,
  match: {
    params: {
      name: string,
    },
  },
};

const withData = WrappedComponent =>
  class extends React.Component<*> {
    constructor(props) {
      super(props);
      this.fetchData = this.fetchData.bind(this);
    }

    componentWillMount() {
      this.fetchData(this.props.match.params.name);
    }

    componentDidUpdate() {
      this.fetchData(this.props.match.params.name);
    }

    fetchData: string => void;
    fetchData(tableName) {
      if (!this.props.data) {
        this.props.getTable(tableName);
      }
    }

    render() {
      const { name } = this.props.match.params;
      return this.props.data && this.props.data.data ? (
        <WrappedComponent
          tableData={this.props.data}
          onHeaderClick={(columnName, order) => this.props.sortData(name, columnName, order)}
          onFilterChange={(columnName, value) => this.props.filterData(name, columnName, value)}
        />
      ) : (
        <Error404 />
      );
    }
  };

const mapStateToProps = (state: StoreState, props: Props) => ({
  data: state[props.match.params.name],
});

const mapDispatchToProps = (dispatch: Dispatch<*>) => ({
  getTable: tableName => dispatch(getTable(tableName)),
  sortData: (tableName, columnName, order) => dispatch(sortData(tableName, columnName, order)),
  filterData: (tableName, columnName, value) => dispatch(filterData(tableName, columnName, value)),
});

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
  withData,
);
