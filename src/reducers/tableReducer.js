// @flow

import type { Actions } from '../actions/tableActions';
import { parseDates, sortByInDirection, normalize, allPass } from '../utils/utils';

export type TableData = {|
  exists: boolean,
  data: ?Array<{}>,
  sortBy?: {
    columnName: string,
    order: "ascending" | "descending",
  },
  filteredData: ?Array<{}>,
  filters: {
    [columnName: string]: string,
  },
|};

export type State = {
  [tableName: string]: TableData,
};

const initialState: State = {};

const tableReducer = (state: State = initialState, action: Actions) => {
  switch (action.type) {
    case 'GET_TABLE': {
      const { data } = action.payload;
      return {
        ...state,
        [action.payload.name]: {
          exists: true,
          data: data ? data.map(parseDates) : null,
          filteredData: null,
          filters: {},
        },
      };
    }
    case 'TABLE_MISSING':
      return {
        ...state,
        [action.payload.name]: {
          exists: false,
          data: null,
          filteredData: null,
          filters: {},
        },
      };
    case 'SORT_DATA': {
      const { data, filteredData } = state[action.payload.tableName];
      const { payload: { columnName, tableName, order } } = action;
      return {
        ...state,
        [tableName]: {
          ...state[tableName],
          sortBy: {
            columnName,
            order,
          },
          data: data ? sortByInDirection(action.payload.order)(
            data,
            columnName,
          ) : null,
          filteredData: filteredData ? sortByInDirection(action.payload.order)(
            filteredData,
            columnName,
          ) : null,
        },
      };
    }
    case 'FILTER_DATA': {
      const { payload: { columnName, tableName, filterValue } } = action;
      const { data } = state[tableName];
      const filters = {
        ...state[tableName].filters,
        [columnName]: filterValue,
      };
      return {
        ...state,
        [tableName]: {
          ...state[tableName],
          filters,
          filteredData: data
            ? data.filter(allPass(Object.entries(filters).map(([key, value]) =>
              datum =>
                normalize(datum[key]).includes(normalize(value))))) : null,
        },
      };
    }
    default: return state;
  }
};

export default tableReducer;
