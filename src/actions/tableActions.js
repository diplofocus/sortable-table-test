// @flow

// eslint-disable-next-line
type _ExtractReturn<B, F: (...args: any[]) => B> = B;
export type ExtractReturn<F> = _ExtractReturn<*, F>;

export const getTable = (tableName: string) => {
  try {
    // Both flow and eslint complain about the next line, flow becase
    // require expects a string literal, and eslint because of global requires.
    // In a real-world scenario this wouldn't be required.
    // $FlowFixMe
    const data = require(`../data/${tableName}.json`); //eslint-disable-line
    return {
      type: 'GET_TABLE',
      payload: {
        name: tableName,
        data,
        exists: true,
      },
    };
  } catch (e) {
    return {
      type: 'TABLE_MISSING',
      payload: {
        name: tableName,
        data: null,
        exists: false,
      },
    };
  }
};

type SortData = (tableName: string, columnName: string, order: 'ascending' | 'descending') => ({
  type: 'SORT_DATA',
  payload: {
    tableName: string,
    columnName: string,
    order: 'ascending' | 'descending',
  },
});

export const sortData = (
  tableName: string,
  columnName: string,
  order: 'ascending' | 'descending',
) => ({
  type: 'SORT_DATA',
  payload: {
    tableName,
    columnName,
    order,
  },
});

export const filterData = (
  tableName: string,
  columnName: string,
  filterValue: string,
) => ({
  type: 'FILTER_DATA',
  payload: {
    tableName,
    columnName,
    filterValue,
  },
});

export type Actions = ExtractReturn<typeof getTable> | SortData;
