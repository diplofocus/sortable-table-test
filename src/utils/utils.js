// @flow
import moment from 'moment';
import type moment$Moment from 'moment';
import removeAccents from 'remove-accents';

// Add formats of supported dates here.
const formats: Array<string> = [
  'YYYY-MM-DD',
  'YYYY-DD-MM',
  'YYYY-MM-DD HH:mm:ss',
  'YYYY-DD-MM HH:mm:ss',
  moment.ISO_8601,
];

const maybeDate = (input: mixed): moment$Moment | string | number | {} | * => {
  if (typeof input === 'object' && input && input.date && input.time) {
    return moment(input).isValid() ? moment(input) : input;
  }
  // $FlowFixMe
  return moment(String(input), formats, true).isValid() ? moment(input) : input;
};

export const normalize = (input: any, preserveNumbers: boolean = false) => {
  switch (typeof input) {
    case 'string':
      return removeAccents(input).toLowerCase();
    case 'number':
      return preserveNumbers ? input : input.toString();
    case 'object': {
      if (preserveNumbers) {
        return moment.isMoment(input) ? input : JSON.stringify(input);
      }
      return moment.isMoment(input) ? input.fromNow() : JSON.stringify(input);
    }
    default:
      return input;
  }
};

export const parseDates = (input: {}) =>
  Object.entries(input).reduce(
    (acc, [key, value]) => ({
      ...acc,
      [key]: maybeDate(value),
    }),
    {},
  );

export const getKey = (input: any) => {
  switch (typeof input) {
    case 'object' || 'array':
      return input.key || JSON.stringify(input);
    default:
      return input;
  }
};

export const sortByInDirection = (direction: "ascending" | "descending",
): ((data: Array<*>, sortingKey: string) => Array<{}>) => (
  data: Array<*>,
  sortingKey: string,
) =>
  (direction === 'ascending'
    ? [...data].sort((a, b) =>
      (normalize(a[sortingKey], true) > normalize(b[sortingKey], true)
        ? 1
        : -1))
    : [...data].sort((a, b) =>
      (normalize(a[sortingKey], true) < normalize(b[sortingKey], true)
        ? 1
        : -1)));

export const allPass = (predicates: Array<(*) => boolean>): ((*) => boolean) =>
  // Flow does not allow passing arguments to functions that don't explicitly
  // expect them. ESLint does not allow unused variables.
  // Compromises.
  predicates.reduce(
    (acc, curr) => (input: *) => acc(input) && curr(input),
    // eslint-disable-next-line no-unused-vars
    x => true,
  );

export const isUrl = (input: string) => {
  // The following regex was copied from the internet. It's functionality is taken at face value.
  const regex = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
  return regex.test(input);
};

export const isEmail = (input: string) => {
  // The following regex was copied from the internet. It's functionality is taken at face value.
  // eslint-disable-next-line no-useless-escape
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(input);
};
