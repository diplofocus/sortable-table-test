// @flow

import { createStore } from 'redux';
import tableReducer from '../reducers/tableReducer';

export default createStore(
  tableReducer,
  // eslint-disable-next-line no-underscore-dangle
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);
