// @flow
import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import humanize from 'string-humanize';

type Props = {};
type State = {
  tableNames: Array<string>,
};

const styles = {
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '10px 0',
  },
  items: {
    padding: 10,
  },
};

class Header extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      tableNames: [],
    };
  }

  componentWillMount() {
    const tableNames: Array<string> = require
    // $FlowFixMe
      .context('../data', true, /\.json$/)
      .keys()
      .map(s => s.slice(2))
      .map(s => s.slice(0, s.length - 5));
    this.setState(() => ({
      tableNames,
    }));
  }

  render() {
    return (
      <div style={styles.container}>
        {this.state.tableNames.map(d => <NavLink key={d} to={`/table/${d}`} style={styles.items}>{humanize(d)}</NavLink>)}
      </div>
    );
  }
}

export default Header;
