// @flow
import React from 'react';
import humanize from 'string-humanize';

import TableFilter from './TableFilter';

type Props = {
  datum: string,
  filterValue: string,
  filterChange: string=> void,
  onClick: void => void,
};

const styles = {
  header: {
    cursor: 'pointer',
  },
};

const TableHeader = ({
  datum, onClick, filterValue, filterChange,
}: Props) => (
  <th>
    <TableFilter value={filterValue} onChange={filterChange} placeholder={`Filter by ${humanize(datum)}`} />
    <div onClick={onClick} style={styles.header} role="button" tabIndex={0}>
      {humanize(datum)}
    </div>
  </th>
);

export default TableHeader;
