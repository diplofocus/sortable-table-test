// @flow

import React from 'react';
import moment from 'moment';
import { getKey, isUrl } from '../utils/utils';

type Element = any | {fromNow?: void => string}
type Props = {
  datum: Element,
};

const styles = {
  padding: 10,
  paddingLeft: 20,
  paddingRight: 20,
};

const formatElement = (element: Element) => {
  switch (typeof element) {
    case 'string': {
      if (isUrl(element)) {
        return (
          <a href={element}>{element}</a>
        );
      }
      return element;
    }
    case 'number': {
      return (
        <div style={{ textAlign: 'right' }}>{(Number.isInteger(element)) ? element : `$${element}`}</div>
      );
    }
    case 'object':
      if (element && moment.isMoment(element) && element.fromNow) {
        return element.fromNow();
      }
      return Object.entries(element).map(([key: string, value: any]) => (
        <div key={getKey(value)}>
          {String(key)}: {String(value)}
        </div>
      ));
    default: return element;
  }
};


const TableCell = ({ datum }: Props) => (
  <td style={styles}>
    {formatElement(datum)}
  </td>
);

export default TableCell;
