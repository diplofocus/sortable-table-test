// @flow
import React from 'react';
import TableHeader from './TableHeader';
import type { TableData } from '../reducers/tableReducer';
import { getKey } from '../utils/utils';
import TableCell from './TableCell';

type Props = {
  onHeaderClick: (tableName: string, columnName: string) => void,
  onFilterChange: (tableName: string, columnName: string) => void,
  tableData: {
    ...TableData,
    data: Array<{
      id: string,
    }>,
  }
};

const Table = ({
  onFilterChange,
  onHeaderClick,
  tableData: {
    data, filteredData, sortBy, filters,
  },
}: Props) => {
  const order = (sortBy && sortBy.order) === 'ascending' ? 'descending' : 'ascending';
  return (
    <div>
      <table style={{ borderCollapse: 'collapse' }}>
        <thead>
          <tr>
            {Object.keys(data[0]).map(columnName => (
              <TableHeader
                filterValue={filters[columnName] || ''}
                filterChange={e => onFilterChange(columnName, e)}
                datum={columnName}
                onClick={() => onHeaderClick(columnName, order)}
                key={getKey(columnName)}
              />
            ))}
          </tr>
        </thead>
        <tbody>
          {(filteredData || data).map(datum => (
            <tr key={getKey(datum)} style={{ borderBottom: '1px solid #ccc' }}>
              {Object.values(datum).map(d => (
                <TableCell key={getKey(d)} datum={d} />
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
