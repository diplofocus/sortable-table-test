// @flow

import * as React from 'react';

type Props = {
  onChange: string => void,
  value: string,
  placeholder: string,
};

const styles = {
  margin: '5px 0 10px 0',
};

class TableFilter extends React.Component<Props> {
  constructor(props: Props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange: (e: SyntheticEvent<HTMLInputElement>) => void;
  handleChange(e: SyntheticEvent<HTMLInputElement>) {
    const v: string = e.currentTarget.value;
    this.props.onChange(v);
  }

  render() {
    const { placeholder, value } = this.props;
    return (
      <input
        style={styles}
        placeholder={placeholder}
        onChange={this.handleChange}
        value={value}
      />
    );
  }
}


export default TableFilter;
