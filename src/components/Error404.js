// @flow

import React from 'react';

const Error404 = () =>
  (
    <div style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
    }}
    >
      <strong style={{ fontSize: 30, color: '#aaa', padding: 20 }}>404</strong>
      <br />
      That&apos;s an error.
      The route
      <code style={{ padding: 10, fontSize: 16 }}>{window.location.pathname}</code>
      does not exist.
    </div>
  );

export default Error404;
