// @flow

import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import Error404 from './components/Error404';
import store from './store/configureStore';
import withData from './hocs/withData';
import Table from './components/Table';
import Header from './components/Header';

require('typeface-roboto');

const styles = {
  fontFamily: 'Roboto',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
};

const App = () =>
  (
    <div style={styles}>
      <Provider store={store}>
        <BrowserRouter>
          <div>
            <Header />
            <Switch>
              <Route path="/" exact component={Error404} />
              <Route path="/table/:name" component={withData(Table)} />
              <Route component={Error404} />
            </Switch>
          </div>
        </BrowserRouter>
      </Provider>
    </div>
  );

export default App;
